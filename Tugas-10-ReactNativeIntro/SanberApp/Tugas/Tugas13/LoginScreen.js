import React, { Component } from 'react';
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput
   } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends Component {
    render(){
      return (
          <View style={styles.container}>
            <View style={styles.logo}>
              <Image source={require('./logo.png')} style={{width:'100%',height:85}} />
              <Text style={{color:'#003366',marginTop:'20%',fontSize:24}}>Login</Text>              
            </View>
            <View style={{marginTop:'7%',marginLeft:'5%',marginRight:'5%'}}>
              <Text>Username</Text>
              <View style={styles.textbox}>
                <TextInput style={styles.txtinput} />
              </View>
              
              <Text style={{marginTop:10}}>Password</Text>
              <View style={styles.textbox}>
                <TextInput style={styles.txtinput} />
              </View>
            </View>
            <View style={styles.button}>
              <TouchableOpacity style={styles.btnDaftar}>
                <Text style={{color:'#ffffff'}}>Daftar</Text>
              </TouchableOpacity>
              <Text style={{marginTop:5,marginBottom:5}}>Atau</Text>
              <TouchableOpacity style={styles.btnLogin}>
                <Text style={{color:'#3EC6FF'}}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: '5%',
      marginTop: '20%'
    },
    logo: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnDaftar: {
      width: 150,
      height: 40,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#003366',
      borderWidth: 1,
      borderRadius: 16,
      borderColor: '#003366'
    },
    btnLogin: {
      width: 150,
      height: 40,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      borderRadius: 16,
      borderColor: '#003366'
    },
    txtinput: {
      width: '100%',
      height: 40,
      borderWidth:1,
      borderColor: '#003366'
    },
    button: {
      marginTop: 25,
      justifyContent: 'center',
      alignItems: 'center'
    },
    textbox: {
      justifyContent: 'center',
      alignItems: 'center'
    }
})
