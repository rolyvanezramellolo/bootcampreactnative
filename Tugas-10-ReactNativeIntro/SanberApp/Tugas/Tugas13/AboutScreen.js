import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput
  } from 'react-native';

export default class App extends Component {
    render(){
      return (
          <View style={styles.container}>
            <View style={styles.header}>
              <Text style={{textAlign:'center',fontSize:20}}>Tentang Saya</Text>
              <View style={styles.photoProfile}>
                <Image source={require('../Tugas13/pp.png')} style={{width:150,height:145}} />
              </View>
              <Text style={{textAlign:'center',fontSize:22,marginTop:'3%'}}>Rolyvan Ezra Mellolo</Text>
              <Text style={{textAlign:'center',fontSize:15,marginTop:'1%'}}>React Native Developer</Text>
            </View>
            <View style={styles.portofolio}>
              <Text style={{marginLeft:10,fontSize:14,marginTop:'3%'}}>Portofolio</Text>
              <View style={styles.icn}>
                <Icon name="gitlab" size={50} />
                <Icon name="github" size={50} />
              </View>
              
            </View>
            <View style={styles.contact}>
              <Text style={{marginLeft:10,fontSize:14,marginTop:'3%'}}>Contact</Text>
              <View style={styles.icn}>
                <Icon name="linkedin-square" size={50} />
                <Icon name="whatsapp" size={50} />
                <Icon name="envelope" size={50} />
              </View>
            </View>
          </View>
          
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: '5%',
      marginTop: '20%'
    },
    header: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    photoProfile: {
      width:150,
      height:150,
      marginTop: '10%',
      borderWidth: 1,
      borderRadius: 180
    },
    portofolio: {
      width: '100%',
      height: 130,
      backgroundColor: '#EFEFEF',
      marginTop: 20,
      marginBottom: 20
    },
    contact: {
      width: '100%',
      height: 130,
      backgroundColor: '#EFEFEF'
    },
    icn: {
      flexDirection: 'row',
      marginTop: 20,
      justifyContent: 'space-around'
    }
})
