import React, { Component } from 'react';
import { 
  Platform,
  StyleSheet, 
  Text, 
  View,
  Image,
  TouchableOpacity,
  FlatList
 } from 'react-native';
import AppTugas12 from './Tugas/Tugas12/YoutubeScreen';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';

export default class App extends Component {
  render(){    
    return (
      <View style={styles.container}>
        {/* <AppTugas12 /> */}
        {/* <LoginScreen /> */}
        <AboutScreen />
      </View>
    );
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
