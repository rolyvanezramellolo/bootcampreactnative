//Fungsi arrow
const golden = () => {
  console.log("this is golden!!")
} 
golden()


//Object literal ES6
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    //fullName: function(){
    	//console.log(firstName + " " + lastName)
    //  return 
    //}
  }
} 
//newFunction("William", "Imoh").fullName()
const person = newFunction("William", "Imoh")
console.log(person.firstName + " " + person.lastName)


//Destructuring
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName)
console.log(lastName)
console.log(destination)
console.log(occupation)


//Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east) /*old*/
const combined = [...west, ...east] /*ES6*/
console.log(combined)


//Template Literals
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)
