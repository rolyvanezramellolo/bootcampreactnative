function arrayToObject(arr) {	
  // Code di sini 
  var now = new Date()
  var thisYear = now.getFullYear() // 2020 (tahun sekarang)
  var i,j;
  var len;
  var obj;
  var rslt = 
   	{
		firstName:"",
		lastName:"",
		gender:"",
		age:""
    };
  for(i = 0; i < arr.length; i++){
  	obj = Object.assign({},arr[i]);
    len = Object.keys(Object.assign({},arr[i])).length;
    if (len < 4){
		rslt.firstName = obj[0];
		rslt.lastName = obj[1];
		rslt.gender = obj[2];
		rslt.age = "Invalid birth year";
    }
    else {
    	rslt.firstName = obj[0];
		rslt.lastName = obj[1];
		rslt.gender = obj[2];
        
		if (parseInt(obj[3]) > thisYear){
			rslt.age = "Invalid birth year";
		}
		else {
			rslt.age = thisYear - parseInt(obj[3]);
		}
    }
    console.log(rslt);
  }	
}

console.log("========== start Array to Object ==========")
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([]) // ""
console.log("========== end Array to Object ==========")



console.log("========== start Shopping Time ==========")
var daftarBarang = 
  [
		{ "barang": "Sepatu brand Stacattu", "harga": 1500000 },
    { "barang": "Casing Handphone", "harga": 50000 },
		{ "barang": "Baju brand Zoro", "harga": 500000 },
    { "barang": "Sweater brand Uniklooh", "harga": 175000 },
		{ "barang": "Baju brand H&N", "harga": 250000 }				
  ];

const sort_by = (field, reverse, primer) => {

  const key = primer ?
    function(x) {
      return primer(x[field])
    } :
    function(x) {
      return x[field]
    };

  reverse = !reverse ? 1 : -1;

  return function(a, b) {
    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
  }
}
daftarBarang.sort(sort_by('harga', true, parseInt))

function shoppingTime(memberId, money) {
	var lstShopping = [], ttlShopping = 0;	
	var rslt;  
  // you can only write your code here!
  if (memberId == '' || memberId == undefined){
  	rslt = "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
  else {
  	if (parseFloat(money) < parseFloat(50000) || money == undefined){
  		rslt = "Mohon maaf, uang tidak cukup";
  	}
    else{
      for(var key in daftarBarang) {
      	if(money > ttlShopping + daftarBarang[key].harga) {
        	lstShopping.push(daftarBarang[key].barang)
      		ttlShopping += daftarBarang[key].harga
        }
      }
      
    	rslt = {
      	memberId: memberId, 
        money: money, 
        listPurchased: lstShopping, 
        changeMoney: (money - ttlShopping)
      };
    }
  }
  return rslt;
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("========== end Shopping Time ==========")



console.log("========== start Naik Angkot ==========")
function naikAngkot(arrPenumpang) {	
	var hasil = [], bayar = 0;
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  for(var i = 0; i < arrPenumpang.length; i++){
  	for(var j = 0; j < rute.length; j++){
    	if(rute[j] == arrPenumpang[i][1])
      {
      	for(var k = (j); k < rute.length; k++){
        	if (rute[k] == arrPenumpang[i][2]){
          	break;
          }
          else {
          	bayar += 2000
          }
        }
        hasil.push("{ penumpang: '" + arrPenumpang[i][0] + "', naikDari: '" + arrPenumpang[i][1] + "', tujuan: '" + arrPenumpang[i][2] + "', bayar: " + bayar + " }")
      }
      else {
      	bayar = 0;
        continue;
      }
    }
  }
  
  return hasil;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])); 
console.log(naikAngkot([])); //[]
console.log("========== end Naik Angkot ==========")