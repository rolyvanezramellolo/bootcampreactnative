var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
var booksQueue = books.length;
var time = 10000;

execute = (time, ind, booksQueue) => {
    readBooksPromise(time, books[ind])
        .then(remainingTime => {
            time = remainingTime;
            booksQueue = booksQueue - 1;
            if (booksQueue > 0) {
                execute(time, ind+1, booksQueue);                
            }
        })
        .catch(error => {
            
        })
}

execute(time, 0, booksQueue);


