import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import indx1 from './index';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <indx1 />
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});