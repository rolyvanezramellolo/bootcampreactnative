function IfElse(){
	var nama = "John"
	var peran = ""
	
	// Output untuk Input nama = '' dan peran = ''
	if (nama == "" && peran == "") {
		console.log("Nama harus diisi!");
	};
	
	//Output untuk Input nama = 'John' dan peran = ''
	if (nama == "John" && peran == ""){
		console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
	};
	
	//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
	if (nama == "Jane" && peran == "Penyihir"){
		console.log("Selamat datang di Dunia Werewolf, " + nama);
		console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
	};
	
	//Output untuk Input nama = 'Jenita' dan peran 'Guard'
	if (nama == "Jenita" && peran == "Guard"){
		console.log("Selamat datang di Dunia Werewolf, " + nama);
		console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
	};
	
	//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
	if (nama == "Junaedi" && peran == "Werewolf"){
		console.log("Selamat datang di Dunia Werewolf, " + nama);
		console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
	};
};

function SwitchCase(){
	var tanggal = "6"; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
	var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
	var tahun = "1987"; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
	
	switch(bulan) {
		case 1:   { console.log(tanggal.toString() + " Januari " + tahun.toString()) ; break; }
		case 2:   { console.log(tanggal.toString() + " Februari " + tahun.toString()) ; break; }
		case 3:   { console.log(tanggal.toString() + " Maret " + tahun.toString()) ; break; }
		case 4:   { console.log(tanggal.toString() + " April " + tahun.toString()) ; break; }
		case 5:   { console.log(tanggal.toString() + " Mei " + tahun.toString()) ; break; }
		case 6:   { console.log(tanggal.toString() + " Juni " + tahun.toString()) ; break; }
		case 7:   { console.log(tanggal.toString() + " Juli " + tahun.toString()) ; break; }
		case 8:   { console.log(tanggal.toString() + " Agustus " + tahun.toString()) ; break; }
		case 9:   { console.log(tanggal.toString() + " September " + tahun.toString()) ; break; }
		case 10:   { console.log(tanggal.toString() + " Oktober " + tahun.toString()) ; break; }
		case 11:   { console.log(tanggal.toString() + " November " + tahun.toString()) ; break; }
		case 12:   { console.log(tanggal.toString() + " Desember " + tahun.toString()) ; break; }
	}	
};