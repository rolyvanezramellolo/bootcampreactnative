function LoopWhile(){
	var flag;

	console.log("LOOPING PERTAMA");
	flag = 1;
	while(flag <= 20) {
		console.log(flag.toString() + " - I love coding");
    flag++
	};

  console.log("LOOPING KEDUA");
	flag = 20;
	while(flag >= 2) {		
		console.log(flag.toString() + " - I will become a mobile developer");
    flag = flag - 2;
	};
};

function LoopFor(){
	var i;
	for (i = 1; i <= 20; i++){
		if(i%2==0){
			console.log(i + " - Berkualitas");
		}
		else
		{
			if(i%3==0){
				console.log(i + " - I Love Coding");
			}
			else {				
        console.log(i + " - Santai");
			}
		}
	}
};

function PersegiPanjang(){
	var i;
	for(i = 0; i < 4; i++){
		console.log("########");
	}		
};

function Tangga(){
	var i;
	for (i = 1; i < 8; i++) {
		var pager = "#".repeat(i);
		console.log(pager);
	};
};

function PapanCatur(){
	var i;
	for (i = 1; i < 9; i++) {
		if (i%2==0){			
			var pager = "# # # #";
			console.log(pager);
		}
		else{
			var pager = "# # # #";
			console.log(" " + pager);
		}
	};
};
