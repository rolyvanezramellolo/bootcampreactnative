console.log("=========== Start Range ===========")
function range(startNum, finishNum) {
	var arr = [];
	var i;
	if (startNum < finishNum) {
		for(i = startNum; i <= finishNum; i++){
			arr.push(i.toString());
		}
	}
	if (startNum > finishNum) {
		for(i = startNum; i >= finishNum; i--){
			arr.push(i.toString());
		}
	}
	if (startNum == null || finishNum == null) {
		arr.push("-1")
	}
	return arr;
};
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("=========== End Range ===========")


console.log("=========== Start Range with Step ===========")
function rangeWithStep(startNum, finishNum, step) {
	var arr = [];
	var i;
	if (startNum < finishNum) {
		for(i = startNum; i <= finishNum; i+=step){
			arr.push(i.toString());
		}
	}
	if (startNum > finishNum) {
		for(i = startNum; i >= finishNum; i-=step){
			arr.push(i.toString());
		}
	}
	if (startNum == null || finishNum == null) {
		arr.push("-1")
	}
	return arr;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log("=========== End Range with Step ===========")


console.log("=========== Start Sum of Range ===========")
function sum(startNum, finishNum, step){
    var arr = [];
	var i, total = 0;
	if (step == null){
		if (startNum < finishNum) {
			for(i = startNum; i <= finishNum; i++){
				total += i;
			}
		}
		if (startNum > finishNum) {
			for(i = startNum; i >= finishNum; i--){
				total += i;
			}
		}
		if (startNum == null || finishNum == null) {
			total = 0;
		}
	}
	if (step != null) {
		if (startNum < finishNum) {
			for(i = startNum; i <= finishNum; i+=step){
				total += i;
			}
		}
		if (startNum > finishNum) {
			for(i = startNum; i >= finishNum; i-=step){
				total += i;
			}
		}
		if (startNum == null || finishNum == null) {
			total = 0;
		}		
	}
	if (startNum != null && finishNum == null && step == null){
		total += startNum;	
	}
	return total;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("=========== End Sum of Range ===========")


console.log("=========== Start Array Multidimensi ===========")
function dataHandling(arr){
	for(var i = 0; i < arr.length; i++){
		console.log("Nomor ID: " + arr[i][0].toString());
		console.log("Nama Lengkap:  " + arr[i][1].toString());
		console.log("TTL: " + arr[i][2].toString());
		console.log("Hobi:  " + arr[i][3].toString());
	}
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
dataHandling(input);
console.log("=========== End Array Multidimensi ===========")


console.log("=========== Start Balik Kata ===========")
function balikKata(kalimat) {
	var hasil = "";
	for (var i = 0; i < kalimat.length; i++){
		hasil += kalimat[(kalimat.length-1)-i];
	}
	
	return hasil;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("=========== End Balik Kata ===========")


console.log("=========== Start Metode Array ===========")
function dataHandling2(arr){
	arr[1] = "Roman Alamsyah Elsharawy";
	arr[2] = "Provinsi Bandar Lampung";
	arr[4] = "Pria";
	arr.push("SMA Internasional Metro");
	console.log(arr);
	
	var bulan = parseInt(arr[3].split("/")[1].toString());
	switch(bulan) {
		case 1:
			console.log("Januari");
			break;
		case 2:
			console.log("Februari");
			break;
		case 3:
			console.log("Maret");
			break;
		case 4:
			console.log("April");
			break;
		case 5:
			console.log("Mei");
			break;
		case 6:
			console.log("Juni");
			break;
		case 7:
			console.log("Juli");
			break;
		case 8:
			console.log("Agustus");
			break;
		case 9:
			console.log("September");
			break;
		case 10:
			console.log("Oktober");
			break;
		case 11:
			console.log("November");
			break;
		case 12:
			console.log("Desember");
	};
	
	var tgl = arr[3].split("/");
	tgl.sort(function (value1, value2) { return value2 - value1 } );
	console.log(tgl);
	
	tgl = arr[3].split("/");
	console.log(tgl.join("-"));
	
	var nama = arr[1].toString();
	console.log(nama.slice(0,14));
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
console.log("=========== End Metode Array ===========")
